<?php

namespace Unit;

use PHPUnit\Framework\TestCase;
use Task\Application;
use Task\ContainerBuilder;
use Task\Http\Request;
use Task\Persitence\DatabaseInterface;

class ApplicationTest extends TestCase
{
    private Application $application;

    private DatabaseInterface $DB;

    protected function setUp(): void
    {
        $this->application = new Application(realpath(__DIR__ . '/../../'), new ContainerBuilder, Request::createFromGlobals());
        $this->application->run();

    }


    public function testAppConfig()
    {
        $config = $this->application->getConfig();
        self::assertArrayHasKey('app', $config);
        self::assertArrayHasKey('APP_ENV', $config['app']);
        self::assertArrayHasKey('database', $config);
        self::assertArrayHasKey('event', $config);
        self::assertArrayHasKey('routes', $config);
    }

}
