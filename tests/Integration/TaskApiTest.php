<?php

namespace Integration;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use PHPUnit\Framework\TestCase;
use Task\Application;
use Task\ContainerBuilder;
use Task\Http\Request;
use Task\Persitence\DatabaseInterface;

class TaskApiTest extends TestCase
{

    private static string $baseUri = 'localhost:8000';
    private static array $clientOptions = ['headers' => [
        'Accept' => 'application/json'
    ]];

    private Client $client;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        $application = new Application(realpath(__DIR__ . '/../../'), new ContainerBuilder, Request::createFromGlobals());
        $application->run();

        $DB = $application->getContainer()->get(DatabaseInterface::class);
        $DB->connection('test')->table('tasks')->delete();
        $DB->connection('test')->table('tasks')->select("delete from sqlite_sequence where name='tasks';");
    }

    protected function setUp(): void
    {
        $this->client = new Client([
            'http_errors' => false,
            'base_uri' => self::$baseUri,
            'headers' => self::$clientOptions['headers']
        ]);
    }

    public function testNotFound()
    {
        $response = $this->client->get('/');

        self::assertEquals(404, $response->getStatusCode());
    }


    public function testCreateTask()
    {
        $response = $this->client->post('/task', [
            RequestOptions::JSON => [
                'id' => 1,
                'title' => 'test',
                'content' => 'test content'
            ]
        ]);
        $data = json_decode($response->getBody()->getContents(), true);
        self::assertEquals(200, $response->getStatusCode());
        self::assertArrayHasKey('success', $data);
        self::assertEquals(true, $data['success']);
    }

    public function testTaskNotFound()
    {
        $response = $this->client->get('/task/999999');

        self::assertEquals(404, $response->getStatusCode());
    }


    public function testUpdateTask()
    {
        $response = $this->client->put('/task/1', [
            RequestOptions::JSON => [
                'title' => 'test',
                'content' => 'test content',
                'status' => 0
            ]
        ]);
        $data = json_decode($response->getBody()->getContents(), true);

        self::assertEquals(200, $response->getStatusCode());
        self::assertArrayHasKey('task', $data);
        self::assertArrayHasKey('title', $data['task']);
        self::assertArrayHasKey('content', $data['task']);
        self::assertArrayHasKey('status', $data['task']);
        self::assertIsNumeric($data['task']['status']);

    }

    public function testListTask()
    {
        $response = $this->client->get('/task');
        $data = json_decode($response->getBody()->getContents(), true);

        self::assertEquals(200, $response->getStatusCode());
        self::assertArrayHasKey('tasks', $data);
        self::assertNotEmpty($data['tasks']);
        self::assertArrayHasKey('title', $data['tasks'][0]);
        self::assertArrayHasKey('content', $data['tasks'][0]);
        self::assertArrayHasKey('status', $data['tasks'][0]);
        self::assertIsNumeric($data['tasks'][0]['status']);
    }

    public function testDeleteTask()
    {
        $response = $this->client->delete('/task/1', [
            RequestOptions::JSON => [
                'title' => 'test',
                'content' => 'test content',
                'status' => 0
            ]
        ]);
        $data = json_decode($response->getBody()->getContents(), true);

        self::assertEquals(200, $response->getStatusCode());
        self::assertArrayHasKey('success', $data);
        self::assertEquals(true, $data['success']);
    }

}
