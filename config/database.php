<?php
return [
    'default' => [
        'driver'   => 'sqlite',
        'database' => realpath(__DIR__ . '/../database/database.sqlite'),
    ],
    'test' => [
        'driver'   => 'sqlite',
        'database' => realpath(__DIR__ . '/../database/test.sqlite'),
    ]
];