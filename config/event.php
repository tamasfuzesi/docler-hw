<?php
return [
    'task.created' => \Task\Event\Listener\TaskCreated::class,
    'task.updated' => \Task\Event\Listener\TaskUpdated::class,
    'task.deleted' => \Task\Event\Listener\TaskDeleted::class,
];