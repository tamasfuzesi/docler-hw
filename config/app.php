<?php
return [
    'APP_NAME' => 'Docler REST API',
    'APP_ENV' => 'test',

    'providers' => [
        \Task\Provider\ExceptionHandlerProvider::class,
        \Task\Provider\HttpServiceProvider::class,
        \Task\Provider\RouteServiceProvider::class,
        \Task\Provider\DatabaseServiceProvider::class,
        \Task\Provider\RepositoryServiceProvider::class,
        \Task\Provider\EventServiceProvider::class,
        \Task\Provider\ValidatorServiceProvider::class,
    ]
];