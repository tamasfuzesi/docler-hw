<?php

use FastRoute\RouteCollector;
use function FastRoute\simpleDispatcher;

return simpleDispatcher(function (RouteCollector $route) {
    $route->get('/task', [\Task\Http\Controller\Task\ListTasks::class]);
    $route->post('/task', \Task\Http\Controller\Task\CreateTask::class);
    $route->get('/task/{id:\d+}', \Task\Http\Controller\Task\GetTask::class);
    $route->put('/task/{id:\d+}', \Task\Http\Controller\Task\UpdateTask::class);
    $route->delete('/task/{id:\d+}', \Task\Http\Controller\Task\DeleteTask::class);
});