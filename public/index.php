<?php


require_once __DIR__. '/../vendor/autoload.php';

use Task\Application;
use Task\ContainerBuilder;
use Task\Http\Request;

$application = new Application(realpath(__DIR__ . '/../'), new ContainerBuilder, Request::createFromGlobals());
$application->run();