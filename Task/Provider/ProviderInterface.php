<?php


namespace Task\Provider;


interface ProviderInterface
{
    public function provide();

    public function register(string $name, $value);
}