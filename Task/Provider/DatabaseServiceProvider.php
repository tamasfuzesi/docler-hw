<?php


namespace Task\Provider;


use Task\Persitence\DatabaseInterface;
use Task\Persitence\EloquentDatabase;

class DatabaseServiceProvider extends ServiceProvider implements ProviderInterface
{
    /**
     * Provide Database handler
     */
    public function provide()
    {
        $database = new EloquentDatabase($this->application->getConfig()['database'], $this->application->getEnvironment());
        $database->initConnections();
        $this->register(DatabaseInterface::class, $database);
    }
}