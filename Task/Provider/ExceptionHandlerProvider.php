<?php


namespace Task\Provider;


class ExceptionHandlerProvider extends ServiceProvider implements ProviderInterface
{
    /**
     * Boot exception handler
     */
    public function provide()
    {
        $whoops = new \Whoops\Run;
        $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
        $whoops->pushHandler(new \Whoops\Handler\JsonResponseHandler);
        $whoops->register();
    }
}