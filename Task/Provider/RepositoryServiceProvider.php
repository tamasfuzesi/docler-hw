<?php


namespace Task\Provider;



use Task\Repository\TaskRepository;
use Task\Repository\TaskRepositoryInterface;

class RepositoryServiceProvider extends ServiceProvider implements ProviderInterface
{
    /**
     * Provide repositories
     */
    public function provide()
    {
        $this->register(TaskRepositoryInterface::class, \DI\autowire(TaskRepository::class));
    }
}