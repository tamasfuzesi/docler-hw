<?php


namespace Task\Provider;


use Task\Validator\TaskValidator;
use Task\Validator\TaskValidatorInterface;

class ValidatorServiceProvider extends ServiceProvider implements ProviderInterface
{
    /**
     * Provide validators
     */
    public function provide()
    {
        $this->register(TaskValidatorInterface::class, new TaskValidator);
    }
}