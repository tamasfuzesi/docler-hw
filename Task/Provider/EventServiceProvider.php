<?php


namespace Task\Provider;


use Task\Event\Event;
use Task\Event\EventInterface;

class EventServiceProvider extends ServiceProvider implements ProviderInterface
{
    /**
     * Provide Event service into container
     * @throws \Task\Event\ListenerIsNotCallableException
     * @throws \ReflectionException
     */
    public function provide()
    {
        $event = new Event($this->application->getConfig()['event']);
        $this->register(EventInterface::class, $event);
    }
}