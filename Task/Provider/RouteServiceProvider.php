<?php


namespace Task\Provider;


use Task\Http\Contract\RouterInterface;
use Task\Http\Router;

class RouteServiceProvider extends ServiceProvider implements ProviderInterface
{
    /**
     * Provide route instance
     */
    public function provide()
    {
        $this->register(RouterInterface::class, new Router($this->application));
    }
}