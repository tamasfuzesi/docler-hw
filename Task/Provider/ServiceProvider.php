<?php


namespace Task\Provider;


use Task\Application;

abstract class ServiceProvider
{
    /**
     * ServiceProvider constructor.
     * @param Application $application
     */
    public function __construct(protected Application $application){/* */}

    /**
     * @param string $name
     * @param $value
     */
    public function register(string $name, $value)
    {
        $this->application->getContainerBuilder()->set($name, $value);
    }
}