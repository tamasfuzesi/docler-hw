<?php


namespace Task\Provider;


use Task\Http\Contract\RequestInterface;

class HttpServiceProvider extends ServiceProvider implements ProviderInterface
{
    /**
     * Provide Request into container
     */
    public function provide()
    {
        $this->register(RequestInterface::class, $this->application->getRequest());
    }
}