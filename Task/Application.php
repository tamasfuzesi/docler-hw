<?php


namespace Task;


use Exception;
use Psr\Container\ContainerInterface;
use ReflectionClass;
use Task\Http\Contract\RequestInterface;
use Task\Http\Contract\RouterInterface;
use Task\Provider\ProviderInterface;

class Application
{
    /**
     * @var string
     */
    private string $configPath;

    /**
     * @var array
     */
    private array $config;

    /**
     * @var ContainerInterface
     */
    private ContainerInterface $container;

    /**
     * Application constructor.
     * @param string $basePath
     * @param ContainerBuilder $containerBuilder
     * @param RequestInterface $request
     */
    public function __construct(
        private string $basePath,
        private ContainerBuilder $containerBuilder,
        private RequestInterface $request)
    {
    }

    /**
     * Application start point
     */
    public function run(): void
    {
        $this->boot();
    }

    /**
     * Booting up
     */
    private function boot(): void
    {
        $this->bootConfig();
        $this->bootProviders();

        $this->buildContainer();
        $this->routerDispatch();
    }

    /**
     * Boot configurations
     */
    private function bootConfig()
    {
        $this->containerBuilder->set('config', $this->readConfigFolder());
    }

    /**
     * Boot app providers
     * @throws \ReflectionException
     */
    private function bootProviders()
    {
        array_map(function ($service) {
            $serviceClass = new ReflectionClass($service);
            $serviceClass = $serviceClass->newInstance($this);
            if($serviceClass instanceof ProviderInterface){
                $serviceClass->provide();
            }
        }, $this->config['app']['providers'] ?? []);

    }

    /**
     * Build DI Container
     * @throws Exception
     */
    private function buildContainer()
    {
        $this->containerBuilder->addDefinitions($this->containerBuilder->getConfig());
        $this->containerBuilder->useAutowiring(true);
        $this->container = $this->containerBuilder->build();
    }

    /**
     * Returns DI Container instance
     * @return ContainerInterface
     */
    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    /**
     * Read configuration folder and load config files
     * @return array
     */
    private function readConfigFolder(): array
    {
        $path = $this->configPath = $this->basePath . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR;
        $dirContent = scandir($path);
        $config = [];

        foreach ($dirContent as $file) {
            if (is_file($path . $file)) {
                $config[basename($file, '.php')] = require $path . $file;
            }
        }

        return $this->config = $config;
    }

    /**
     * Get config path
     * @return string
     */
    public function getConfigPath(): string
    {
        return $this->configPath;
    }

    /**
     * Returns app configurations
     * @return array
     */
    public function getConfig(): array
    {
        return $this->config;
    }

    /**
     * Returns app base path
     * @return string
     */
    public function getBasePath(): string
    {
        return $this->basePath;
    }

    /**
     * Get DI Container builder
     * @return ContainerBuilder
     */
    public function getContainerBuilder(): ContainerBuilder
    {
        return $this->containerBuilder;
    }

    /**
     * Returns app requests
     * @return RequestInterface
     */
    public function getRequest(): RequestInterface
    {
        return $this->request;
    }

    /**
     * Dispatch router
     */
    private function routerDispatch()
    {
        $this->container->get(RouterInterface::class)->dispatch();
    }

    /**
     * @return string
     */
    public function getEnvironment(): string
    {
        return $this->getConfig()['app']['APP_ENV'];
    }
}