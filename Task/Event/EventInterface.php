<?php


namespace Task\Event;


interface EventInterface
{
    public function listen(string $eventName, callable $listener);

    public function dispatch($eventName, $data);
}