<?php


namespace Task\Event;


use ReflectionClass;

class Event implements EventInterface
{
    /**
     * @var array
     */
    private array $events = [];

    /**
     * Event constructor.
     * @param array|null $config
     * @throws ListenerIsNotCallableException|\ReflectionException
     */
    public function __construct(?array $config = [])
    {
        $this->setListenersFromConfig($config);
    }

    /**
     * @param string $eventName
     * @param $listener
     * @throws ListenerIsNotCallableException|\ReflectionException
     */
    public function listen(string $eventName, callable|array|string|ListenerInterface $listener)
    {
        $listener = is_string($listener) ? $this->makeCallableObject($listener) : $listener;

        if(is_array($listener)){
            array_map(function($listener){
                if(! is_callable($listener)){
                    throw new ListenerIsNotCallableException;
                }
            }, $listener);
            $this->events[$eventName] = $listener;
            return;
        }

        $this->events[$eventName][] = $listener;
    }

    /**
     * @param $eventName
     * @param $data
     */
    public function dispatch($eventName, $data)
    {
        array_map(fn($listener) => $listener($data), $this->events[$eventName]);
    }

    /**
     * @param array $config
     * @throws ListenerIsNotCallableException
     * @throws \ReflectionException
     */
    private function setListenersFromConfig(array $config)
    {
        foreach ($config as $eventName => $listeners){
            $this->listen($eventName, $listeners);
        }
    }

    /**
     * @param string $listener
     * @return object
     * @throws \ReflectionException
     * @throws ListenerIsNotCallableException
     */
    private function makeCallableObject(string $listener): object
    {
        if(! class_exists($listener)){
            throw new ListenerIsNotCallableException("Class [{$listener}] is not available");
        }

        $class = new ReflectionClass($listener);

        if(is_callable($class)){
            throw new ListenerIsNotCallableException("Class [{$listener}] is not callable");
        }

        return $class->newInstance();
    }
}
