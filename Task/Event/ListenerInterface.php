<?php


namespace Task\Event;


interface ListenerInterface
{
    public function __invoke();
}