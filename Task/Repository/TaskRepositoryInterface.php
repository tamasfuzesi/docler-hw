<?php


namespace Task\Repository;


interface TaskRepositoryInterface
{
    public function getById(int $id);

    public function create(array $data);

    public function updateById(int $id, array $data);

    public function deleteById(int $id);

    public function all();
}