<?php


namespace Task\Repository;


use Task\Event\EventInterface;
use Task\Model\Task;
use Task\Model\TaskCollection;
use Task\Persitence\DatabaseInterface;

class TaskRepository implements TaskRepositoryInterface
{
    /**
     * TaskRepository constructor.
     * @param DatabaseInterface $DB
     * @param EventInterface $event
     */
    public function __construct(private DatabaseInterface $DB, private EventInterface $event) {}

    /**
     * Get Task by id
     * @param int $id
     * @return Task
     * @throws \Task\Model\TaskStatus\InvalidTaskStatusException
     * @throws ResourceNotFoundException
     */
    public function getById(int $id): Task
    {
        $result = $this->DB->connection()->table('tasks')
            ->where('id', $id)
            ->first();

        if(! $result){
            throw new ResourceNotFoundException("Task [{$id}] not found");
        }

        return Task::make($result);
    }

    /**
     * Create Task
     * @param array $data
     * @return mixed
     */
    public function create(array $data): mixed
    {
        $task = $this->DB->connection()->table('tasks')
            ->insert($data);

        $this->event->dispatch('task.created', $task);

        return $task;
    }

    /**
     * Update Task by id
     * @param int $id
     * @param array $data
     * @return Task
     * @throws \Task\Model\TaskStatus\InvalidTaskStatusException|ResourceNotFoundException
     */
    public function updateById(int $id, array $data): Task
    {
        $this->DB->connection()->table('tasks')
            ->where('id', $id)
            ->update($data);

        $task = $this->getById($id);

        $this->event->dispatch('task.updated', $task);

        return $task;
    }

    /**
     * Delete Task by id
     * @param int $id
     * @return mixed
     */
    public function deleteById(int $id): mixed
    {
        $deleted = $this->DB->connection()->table('tasks')
            ->where('id', $id)
            ->delete();

        $this->event->dispatch('task.deleted', $deleted);

        return $deleted;
    }

    /**
     * Returns all Task
     * @return TaskCollection
     */
    public function all(): TaskCollection
    {
        $result = $this->DB->connection()->table('tasks')
            ->get();

        return TaskCollection::collect($result->toArray());
    }
}