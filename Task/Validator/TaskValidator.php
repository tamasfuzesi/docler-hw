<?php


namespace Task\Validator;



use Exception;
use Task\Model\TaskStatus\ClosedTaskStatus;
use Task\Model\TaskStatus\DeletedTaskStatus;
use Task\Model\TaskStatus\InProgressTaskStatus;
use Task\Model\TaskStatus\OpenTaskStatus;

class TaskValidator implements TaskValidatorInterface
{
    /**
     * @var array
     */
    private array $errors = [];

    /**
     * Validate inputs
     * @param array $data
     */
    public function validate(array $data)
    {
        try {
            $this->validateTitle($data['title']);
            $this->validateContent($data['content']);
            $this->validateStatus($data['status'] ?? null);
        } catch (Exception $exception){
            $this->errors[] = $exception->getMessage();
        }
    }

    /**
     * Returns errors messages
     * @return array
     */
    public function fails(): array
    {
        return ['errors' => $this->errors];
    }

    /**
     * Validate title of task data
     * @param string|null $title
     * @throws InvalidDataException
     */
    public function validateTitle(?string $title = null)
    {
        if(! $title){
            throw new InvalidDataException('title', 'Task title is required');
        }
    }

    /**
     * Validate content of task data
     * @param string|null $content
     * @throws InvalidDataException
     */
    public function validateContent(?string $content = null)
    {
        if(! $content){
            throw new InvalidDataException('content', 'Task content is required');
        }
    }

    /**
     * Validate task's status
     * @param int|null $status
     * @throws InvalidDataException
     */
    public function validateStatus(?int $status = null)
    {

        if($status && ! in_array($status, [
            OpenTaskStatus::VALUE,
            InProgressTaskStatus::VALUE,
            DeletedTaskStatus::VALUE,
            ClosedTaskStatus::VALUE
        ])){
            throw new InvalidDataException('status', 'Invalid status: '. $status);
        }
    }

    /**
     * Returns true if data is valid
     * @return bool
     */
    public function isValid(): bool
    {
        return ! (bool) count($this->errors);
    }
}