<?php


namespace Task\Validator;


interface TaskValidatorInterface
{
    public function validate(array $data);

    public function fails(): array;

    public function isValid(): bool;
}