<?php


namespace Task\Validator;


use Exception;
use Throwable;

class InvalidDataException extends Exception
{
    public function __construct(private ?string $input = '', $message = '', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * Returns input name
     * @return string
     */
    public function input(): string
    {
        return $this->key;
    }
}