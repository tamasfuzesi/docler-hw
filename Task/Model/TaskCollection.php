<?php


namespace Task\Model;


use Task\Model\Contract\Arrayable;

class TaskCollection implements Arrayable
{
    private array $items;

    public static function collect(array $items): TaskCollection
    {
        $taskCollection = new self;
        $taskCollection->setItems(array_map(fn($item) => Task::make($item), $items));
        return $taskCollection;
    }

    /**
     *
     * @return int
     */
    public function count(): int
    {
        return count($this->items);
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param array $items
     */
    public function setItems(array $items): void
    {
        $this->items = $items;
    }

    /**
     * Convert object to array
     * @return array
     */
    public function toArray(): array
    {
        return array_map(fn(Task $item) => $item->toArray(), $this->items);
    }
}