<?php


namespace Task\Model\TaskStatus;


class InProgressTaskStatus implements TaskStatusInterface
{
    /**
     * @const int
     */
    const VALUE = 1;

    /**
     * @return int
     */
    public function __invoke(): int
    {
        return self::VALUE;
    }

    /**
     * Returns readable value
     * @return string
     */
    public function __toString(): string
    {
        return 'Folyamatban';
    }

    /**
     * Get persistence value
     * @return int
     */
    public function getValue(): int
    {
        return self::VALUE;
    }
}