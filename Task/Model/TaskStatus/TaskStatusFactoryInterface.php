<?php


namespace Task\Model\TaskStatus;


interface TaskStatusFactoryInterface
{
    public function createOpenTaskStatus(): TaskStatusInterface;

    public function createInProgressTaskStatus(): TaskStatusInterface;

    public function createDeletedTaskStatus(): TaskStatusInterface;

    public function createClosedTaskStatus(): TaskStatusInterface;
}