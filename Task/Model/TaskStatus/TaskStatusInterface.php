<?php


namespace Task\Model\TaskStatus;


interface TaskStatusInterface
{
    public function __invoke(): int;

    public function __toString(): string;

    public function getValue(): int;
}