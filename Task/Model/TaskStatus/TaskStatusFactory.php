<?php


namespace Task\Model\TaskStatus;


class TaskStatusFactory implements TaskStatusFactoryInterface
{
    /**
     * Create TaskStatus instance
     * @param int $status
     * @return TaskStatusInterface
     * @throws InvalidTaskStatusException
     */
    public static function create(int $status): TaskStatusInterface
    {
        $taskStatusFactory = new TaskStatusFactory;
        return match ($status){
            OpenTaskStatus::VALUE => $taskStatusFactory->createOpenTaskStatus(),
            InProgressTaskStatus::VALUE => $taskStatusFactory->createInProgressTaskStatus(),
            DeletedTaskStatus::VALUE => $taskStatusFactory->createDeletedTaskStatus(),
            ClosedTaskStatus::VALUE => $taskStatusFactory->createClosedTaskStatus(),
            default => throw new InvalidTaskStatusException("Invalid TaskStatus")
        };
    }

    /**
     * @return TaskStatusInterface
     */
    public function createOpenTaskStatus(): TaskStatusInterface
    {
        return new OpenTaskStatus();
    }

    /**
     * @return TaskStatusInterface
     */
    public function createInProgressTaskStatus(): TaskStatusInterface
    {
        return new InProgressTaskStatus();
    }

    /**
     * @return TaskStatusInterface
     */
    public function createDeletedTaskStatus(): TaskStatusInterface
    {
        return new DeletedTaskStatus();
    }

    /**
     * @return TaskStatusInterface
     */
    public function createClosedTaskStatus(): TaskStatusInterface
    {
        return new ClosedTaskStatus();
    }
}