<?php


namespace Task\Model\TaskStatus;



class OpenTaskStatus implements TaskStatusInterface
{
    /**
     * @const int
     */
    const VALUE = 0;

    /**
     * @return int
     */
    public function __invoke(): int
    {
        return self::VALUE;
    }

    /**
     * Returns readable value
     * @return string
     */
    public function __toString(): string
    {
        return 'Új';
    }

    /**
     * Get persistence value
     * @return int
     */
    public function getValue(): int
    {
        return self::VALUE;
    }
}