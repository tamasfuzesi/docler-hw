<?php


namespace Task\Model\TaskStatus;


class ClosedTaskStatus implements TaskStatusInterface
{
    /**
     * @const int
     */
    public const VALUE = 3;

    /**
     * @return int
     */
    public function __invoke(): int
    {
        return self::VALUE;
    }

    /**
     * Returns readable value
     * @return string
     */
    public function __toString(): string
    {
        return 'Lezárvaa';
    }

    /**
     * Get persistence value
     * @return int
     */
    public function getValue(): int
    {
        return self::VALUE;
    }
}