<?php


namespace Task\Model\TaskStatus;


class DeletedTaskStatus implements TaskStatusInterface
{
    /**
     * int VALUE
     */
    const VALUE = 2;

    /**
     * @return int
     */
    public function __invoke(): int
    {
        return self::VALUE;
    }

    /**
     * Returns readable value
     * @return string
     */
    public function __toString(): string
    {
        return 'Törölt';
    }

    /**
     * Get persistence value
     * @return int
     */
    public function getValue(): int
    {
        return self::VALUE;
    }
}