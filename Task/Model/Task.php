<?php


namespace Task\Model;


use Task\Model\Contract\Arrayable;
use Task\Model\TaskStatus\TaskStatusFactory;
use Task\Model\TaskStatus\TaskStatusInterface;

class Task implements Arrayable
{
    /**
     * @var int
     */
    public int $id;

    /**
     * @var string
     */
    private string $title;

    /**
     * @var string
     */
    private string $content;

    /**
     * @var TaskStatusInterface
     */
    private TaskStatusInterface $status;

    /**
     * Make new Task instance by request data
     * @param $data
     * @return Task
     * @throws TaskStatus\InvalidTaskStatusException
     */
    public static function make($data): Task
    {
        $task = new self;
        $task->setTitle($data->title);
        $task->setId($data->id);
        $task->setContent($data->content);

        $task->setStatus(TaskStatusFactory::create($data->status));

        return $task;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return TaskStatusInterface
     */
    public function getStatus(): TaskStatusInterface
    {
        return $this->status;
    }

    /**
     * @param TaskStatusInterface $status
     */
    public function setStatus(TaskStatusInterface $status): void
    {
        $this->status = $status;
    }

    /**
     * Convert object to array
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'content' => $this->content,
            'status' => $this->status->getValue()
        ];
    }
}