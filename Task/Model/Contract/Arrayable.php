<?php


namespace Task\Model\Contract;


interface Arrayable
{
    public function toArray(): array;
}