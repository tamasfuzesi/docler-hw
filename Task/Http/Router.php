<?php


namespace Task\Http;


use FastRoute\Dispatcher;
use Task\Application;
use Task\Http\Contract\RouterInterface;
use Task\Http\Controller\Error\MethodNotAllowed;
use Task\Http\Controller\Error\NotFound;
use Task\Repository\ResourceNotFoundException;

class Router implements RouterInterface
{
    /**
     * Router constructor.
     * @param Application $application
     */
    public function __construct(private Application $application) {/* */}

    /**
     * Router dispatcher
     */
    public function dispatch()
    {
        $dispatcher = $this->getDispatcher();
        $routeInfo = $dispatcher->dispatch(
            $this->application->getRequest()->getMethod(),
            $this->application->getRequest()->getRequestUri()
        );

        switch ($routeInfo[0]) {
            case Dispatcher::NOT_FOUND:
                $this->application->getContainer()->call(NotFound::class);
                break;
            case Dispatcher::METHOD_NOT_ALLOWED:
                $this->application->getContainer()->call(MethodNotAllowed::class);
                break;
            case Dispatcher::FOUND:
                $this->callController($routeInfo);

                break;
        }
    }

    /**
     * Get router dispatcher configuration
     * @return mixed
     */
    public function getDispatcher()
    {
        return $this->application->getConfig()['routes'];
    }

    private function callController($routeInfo)
    {
        list($state, $handler, $vars) = $routeInfo;
        unset($state);

        if(is_array($handler)){
            list($class, $method) = count($handler) === 2 ? $handler : [...$handler, null];
        } else {
            $class = $handler;
        }

        try{
            if(isset($method)){
                $controller = $this->application->getContainer()->get($class);
                $controller->{$method}(...$vars);
            } else {
                $this->application->getContainer()->call($class, $vars);
            }
        } catch(ResourceNotFoundException $exception){
            $this->application->getContainer()->call(NotFound::class);
        }
    }


}