<?php


namespace Task\Http;


use Task\Http\Contract\RequestInterface;
use Task\Http\Contract\ResponseInterface;

class Response implements ResponseInterface
{
    /**
     * Response constructor.
     * @param RequestInterface $request
     * @param null $data
     * @param int $status
     * @param array $headers
     */
    public function __construct(
        private RequestInterface $request,
        private $data = null,
        private int $status = 200,
        private array $headers = [])
    {
        $this->send();
    }

    /**
     * Send data to output
     */
    public function send()
    {
        match ($this->request->getHeader('Accept')){
            'application/json' => ResponseFactory::createJsonResponse($this->data, $this->status, $this->headers),
            default => ResponseFactory::createPlainTextResponse($this->data, $this->status, $this->headers)
        };
    }
}