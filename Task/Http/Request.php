<?php


namespace Task\Http;


use Task\Http\Contract\RequestInterface;
use \Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class Request extends SymfonyRequest implements RequestInterface
{
    /**
     * All request data
     * @return array
     */
    public function all(): array
    {
        if($this->getHeader('Content-Type') === 'application/json'){
            return json_decode($this->getContent(), true);
        }

        return $this->request->all();
    }

    /**
     * Get header by key
     * @param string $string
     * @return string|null
     */
    public function getHeader(string $string): ?string
    {
        return $this->headers->get($string);
    }
}