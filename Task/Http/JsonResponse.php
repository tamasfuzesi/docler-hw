<?php


namespace Task\Http;


use Task\Http\Contract\JsonResponseInterface;

class JsonResponse extends \Symfony\Component\HttpFoundation\JsonResponse implements JsonResponseInterface
{
    /**
     * JsonResponse constructor.
     * @param null $data
     * @param int $status
     * @param array $headers
     * @param bool $json
     */
    public function __construct($data = null, int $status = 200, array $headers = [], bool $json = false)
    {
        parent::__construct($data, $status, $headers, $json);
        $this->send();
    }
}