<?php


namespace Task\Http\Controller\Task;


use Task\Http\Contract\RequestInterface;
use Task\Http\Response;
use Task\Repository\TaskRepositoryInterface;

class ListTasks
{
    /**
     * ListTasks constructor.
     * @param RequestInterface $request
     * @param TaskRepositoryInterface $taskRepository
     */
    public function __construct(private RequestInterface $request, private TaskRepositoryInterface $taskRepository) {}

    /**
     * @return Response
     */
    public function __invoke(): Response
    {
        return new Response($this->request, ['tasks' => $this->taskRepository->all()->toArray()]);
    }
}