<?php


namespace Task\Http\Controller\Task;


use Task\Http\Contract\RequestInterface;
use Task\Http\Response;
use Task\Repository\TaskRepositoryInterface;

class DeleteTask
{
    /**
     * DeleteTask constructor.
     * @param RequestInterface $request
     * @param TaskRepositoryInterface $taskRepository
     */
    public function __construct(private RequestInterface $request, private TaskRepositoryInterface $taskRepository) {}

    /**
     * @param int $id
     * @return Response
     */
    public function __invoke(int $id): Response
    {
        return new Response($this->request, ['success' => $this->taskRepository->deleteById($id)]);
    }
}