<?php


namespace Task\Http\Controller\Task;


use Task\Http\Contract\RequestInterface;
use Task\Http\Response;
use Task\Repository\TaskRepositoryInterface;
use Task\Validator\TaskValidatorInterface;

class CreateTask
{
    /**
     * CreateTask constructor.
     * @param RequestInterface $request
     * @param TaskRepositoryInterface $taskRepository
     * @param TaskValidatorInterface $taskValidator
     */
    public function __construct(
        private RequestInterface $request,
        private TaskRepositoryInterface $taskRepository,
        private TaskValidatorInterface $taskValidator) {}


    /**
     * @return Response
     */
    public function __invoke(): Response
    {
        $this->taskValidator->validate($this->request->all());
        if(! $this->taskValidator->isValid()) {
            return new Response($this->request, $this->taskValidator->fails(), 422);
        }

        $task = $this->taskRepository->create($this->request->all());
        return new Response($this->request, ['success' => $task]);
    }
}