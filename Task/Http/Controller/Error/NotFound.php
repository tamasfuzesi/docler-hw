<?php


namespace Task\Http\Controller\Error;


use Task\Http\Contract\RequestInterface;
use Task\Http\Response;

class NotFound
{
    /**
     * NotFound constructor.
     * @param RequestInterface $request
     */
    public function __construct(private RequestInterface $request) {}

    /**
     * @return Response
     */
    public function __invoke(): Response
    {
        return new Response($this->request, ['error' => 404, 'message' => 'Not found'], 404);
    }
}