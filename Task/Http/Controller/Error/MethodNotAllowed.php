<?php


namespace Task\Http\Controller\Error;


use Task\Http\Contract\RequestInterface;
use Task\Http\Response;

class MethodNotAllowed
{
    /**
     * MethodNotAllowed constructor.
     * @param RequestInterface $request
     */
    public function __construct(private RequestInterface $request) {}

    /**
     * @return Response
     */
    public function __invoke(): Response
    {
        return new Response($this->request, ['error' => 405, 'message' => 'Method not allowed.'], 405);
    }
}