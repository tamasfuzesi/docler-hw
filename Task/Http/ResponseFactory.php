<?php


namespace Task\Http;


use Task\Http\Contract\JsonResponseInterface;

class ResponseFactory
{
    /**
     * Create JsonResponse response output
     * @param null $data
     * @param int $status
     * @param array $headers
     * @param bool $json
     * @return JsonResponseInterface
     */
    public static function createJsonResponse($data = null, int $status = 200, array $headers = [], bool $json = false): JsonResponseInterface
    {
        return new JsonResponse($data, $status, $headers, $json);
    }

    /**
     * Create text/plain response output
     * @param $content
     * @param int $status
     * @param array $headers
     * @return PlainTextResponse
     */
    public static function createPlainTextResponse($content, int $status = 200, array $headers = [])
    {
        $content = ! is_string($content) ? print_r($content) : $content;

        return new PlainTextResponse($content, $status, $headers);
    }
}