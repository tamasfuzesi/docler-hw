<?php


namespace Task\Http;


use Task\Http\Contract\ResponseInterface;
use \Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class PlainTextResponse extends SymfonyResponse implements ResponseInterface
{

}