<?php


namespace Task\Http\Contract;


interface RequestInterface
{
    public function getMethod();

    public function getRequestUri();

    public function all();

    public function getHeader(string $string);
}