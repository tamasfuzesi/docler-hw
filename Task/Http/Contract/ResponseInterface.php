<?php


namespace Task\Http\Contract;


interface ResponseInterface
{
    public function send();
}