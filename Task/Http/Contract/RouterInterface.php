<?php


namespace Task\Http\Contract;


interface RouterInterface
{
    public function dispatch();
}