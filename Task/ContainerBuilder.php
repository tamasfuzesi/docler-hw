<?php


namespace Task;


use DI\ContainerBuilder as DIContainerBuilder;

class ContainerBuilder extends DIContainerBuilder
{
    /**
     * @var array
     */
    private array $config;

    /**
     * Set instance to DI ContainerBuilder
     * @param string $name
     * @param $value
     */
    public function set(string $name, $value): void
    {
        $this->config[$name] = $value;
    }

    /**
     * Get ContainerBuilder configurations
     * @return array
     */
    public function getConfig(): array
    {
        return $this->config;
    }
}