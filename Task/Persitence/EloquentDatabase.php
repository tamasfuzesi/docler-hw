<?php


namespace Task\Persitence;


use Illuminate\Database\Capsule\Manager;
use Task\Application;

class EloquentDatabase implements DatabaseInterface
{
    /**
     * @var Manager
     */
    private Manager $manager;

    /**
     * EloquentDatabase constructor.
     * @param array $databaseConfig
     * @param string $environment
     */
    public function __construct(private array $databaseConfig, private string $environment)
    {
        $this->manager = new Manager;
    }

    /**
     * Initialize database connection
     */
    public function initConnections()
    {
        foreach ($this->databaseConfig as $databaseName => $config){
            $this->manager->addConnection($config, $databaseName);
        }
    }

    /**
     * @param string|null $connection
     * @return \Illuminate\Database\Connection
     * @throws DatabaseNotExistsException
     */
    public function connection(?string $connection = null)
    {
        $connection = $this->environment === 'test' ? 'test' : ($connection ?? 'default');

        if(! $this->isConnectionExists($connection)){
            throw new DatabaseNotExistsException("EloquentDatabase [{$connection}] connection is not exists in config file.");
        }

        return $this->manager->getConnection($connection);
    }

    /**
     * Returns true if connection's configuration is available
     * @param string $connection
     * @return bool
     */
    public function isConnectionExists(string $connection): bool
    {
        return array_key_exists($connection, $this->databaseConfig);
    }

}