<?php


namespace Task\Persitence;


interface DatabaseInterface
{
    public function connection(?string $connection = null);
}