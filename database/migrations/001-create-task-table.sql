CREATE TABLE tasks
(
    id      INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    title   TEXT,
    content TEXT,
    status  INTEGER DEFAULT 0
);
